/*JavaScript For Preventing animation when resizing*/
var resizeTimer;

window.addEventListener("resize", function () {
    document.body.classList.add("resize-animation-stopper");
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
        document.body.classList.remove("resize-animation-stopper");
    }, 400);
});

/*JavaScript For Drop down menu*/
var NaviconBtn = document.getElementById("NaviconBtn");
    NaviconBtn.addEventListener("click", function (){
        var DrpMenu = document.getElementById("TabPhoneNavUL");
        var DrpMenuClHeight = DrpMenu.clientHeight;

        if (DrpMenuClHeight == 0) {
            DrpMenu.style.height = "55vh";
            DrpMenu.style.opacity = "1";
            DrpMenu.style.transition = "opacity 1s";
            NaviconBtn.style.backgroundImage = 'url("Image/Xicon.png")';
        }
        else {
            DrpMenu.style.height = "0vw";
            DrpMenu.style.opacity = "0";
            NaviconBtn.style.backgroundImage = 'url("Image/BurgerIcn.png")';
            DrpMenu.style.transition = "opacity 0s";
        }
    });

    var body      = document.getElementsByTagName("body")[0];
    body.addEventListener("load",SetFooter());
    window.onresize = SetFooter;

/*JavaScript To set footer always at bottom*/
  function SetFooter(){
     var body      = document.getElementsByTagName("body")[0];
     var bodyScrollHeight = body.scrollHeight;
     var DeviceHeight = window.innerHeight;
     var footer       = document.getElementById("Footer");
     if(bodyScrollHeight >= DeviceHeight){
         footer.style.position = "static";
         footer.style.bottom   = "auto";
    }
    else{
         footer.style.position = "fixed";
         footer.style.bottom   = "0";
     }
  }