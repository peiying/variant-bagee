
/*
Author: Teo Shi Han
Page  : Checkout page and Payment page
Basic function: 1. Control the behaviour of the form
                2. Achieve Java-Pattern hybrid validation for the form
                3. Prevent enter key from submit the form
*/

//------------BELOW SCRIPT IS FOR STYLING FOR FORM FOR CONDITION LIKE FOCUSIN,ERROR--------------//
function StartInputStyle(FormID, ButtonID) {
   var Form = document.getElementById(FormID);
   var AllInput = Form.querySelectorAll("input");
   AllInput.forEach(InputElement => {

      //Normal Focus Styling(Trigger when user focus field)
      InputElement.addEventListener("focusin", function () {
         DisplayNormalFocus(InputElement);

         //when enter && null, red the field
         InputElement.addEventListener("keyup", function (TheKey) {
            IndicateEnteredNULL(InputElement, TheKey);
         });

         //=> data here 100% not null
         if (ContainError(FormID) == false) {
            //Form 100% not contain error => Enter trigger press purchase button, not submit
            //submit cannot direct to page
            InputElement.addEventListener("keydown", function (TheKey) {
               //if enter pressed, we dont submit,we press the purchase button instead
               //purchase button will submit
               if (TheKey.code == "Enter" || TheKey.code == "NumpadEnter") {
                  document.getElementById(ButtonID).click();
               }
            });
         }
         else {
            //Form 100% contain error => Enter trigger submit that highlight next error
            InputElement.addEventListener("change", function (TheKey) {
               if (InputElement.checkValidity() == false) {
                  //If this field error:
                  //=> Detect enter key, Enter => Trigger validate(because submit)
                  //=> focus red error field(this field);
                  IndicateInvalidInput(InputElement, TheKey);
               }
               //else, do nothing, The null field at below no need red(empty)
            });
         }

         InputElement.addEventListener("focusout", function () {
            if (InputElement.checkValidity() == false) {
               DisplayInvalidFocus(InputElement);
            }
            else {
               DisplayValidFocus(InputElement);
               if (FormID == "BillingDetails") {
                  AllInput[((AllInput.length) - 1)].removeAttribute("style");
               }
            }
         });
         if (FormID == "BillingDetails") {
            AllInput[((AllInput.length) - 1)].removeAttribute("style");
         }
      });
   })
};

//Normal focus style
function DisplayNormalFocus(SelectedElement) {
   SelectedElement.style.outline = "1.5px #cfbea5 solid";
   SelectedElement.style.backgroundColor = "#fcfcf6";
};
//Error input focus style
function DisplayInvalidFocus(SelectedElement) {
   SelectedElement.style.outline = "1.5px red solid";
   SelectedElement.style.backgroundColor = "pink";
};

function DisplayValidFocus(SelectedElement) {
   SelectedElement.style.outline = "1.5px green solid";
   SelectedElement.style.backgroundColor = "#d3f8d3";
};

function IndicateEnteredNULL(SelectedElement, TheKey) {
   if ((TheKey.code == "Enter" && SelectedElement.value == "") || (TheKey.code == "NumpadEnter" && SelectedElement.value == "")) {
      DisplayInvalidFocus(SelectedElement);
   }
};

function IndicateInvalidInput(SelectedElement, TheKey) {
   SelectedElement.addEventListener("keydown", function (TheKey) {
      if (TheKey.code == "Enter" || TheKey.code == "NumpadEnter") {
         DisplayInvalidFocus(SelectedElement);
      }
   });
}

function ContainError(FormID) {
   Form = document.getElementById(FormID);
   AllInput = Form.getElementsByTagName('input');
   for (i = 0; i < AllInput.length; i++) {
      if (AllInput[i].checkValidity() == false) {
         var ContainError = true;
         break;
      }
      else {
         ContainError = false;
      }
   }
   return ContainError;
}